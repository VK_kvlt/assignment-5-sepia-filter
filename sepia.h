#ifndef __SEPIA_H__
#define __SEPIA_H__

#include "image.h"

void sepia(struct image* imgi, struct image* imgo);


#endif
